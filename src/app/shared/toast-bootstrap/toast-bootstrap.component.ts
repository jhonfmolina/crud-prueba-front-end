import { Component } from '@angular/core';
import { ToastService } from './toast-bootstrap.service';

@Component({
  selector: 'app-toast-bootstrap',
  templateUrl: './toast-bootstrap.component.html',
  styleUrls: ['./toast-bootstrap.component.scss']
})
export class ToastBootstrapComponent {

  public estado : boolean = false;
  public data : any;
  constructor(private _toastService : ToastService){
    this._toastService.toast.subscribe(
      (resp:any) => {        
        setTimeout(() => {
          this.data = resp;
          this.estado = true;
          console.log('compo');
          
        }, 0);
        setTimeout(() => {
          this.estado = false;
        }, 5000);
      }
    );
  }

}
