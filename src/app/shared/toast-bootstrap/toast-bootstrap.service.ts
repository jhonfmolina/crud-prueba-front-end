import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ToastService {

    public toast = new EventEmitter<any>();

    public customMsj = {
        verificarForm: "Validar los datos del formulario."
    }

    constructor() {
    }


    typeToast = (type: string, mensaje: string) => {
        
        if (type === "info") {
            this.toast.emit({
                type: 'Algo te hace falta',
                bg: 'bg-warning',
                icon: 'bi bi-emoji-wink-fill',
                text: 'text-warning',
                message: mensaje
            })
        }
        if (type === "error") {            
            this.toast.emit({
                type: 'Algo salió mal',
                bg: 'bg-danger',
                icon: 'bi bi-emoji-frown-fill',
                text: 'text-danger',
                message: mensaje
            })
        }
        if (type === "success") {
            this.toast.emit({
                type: 'Todo salio perfecto!',
                bg: 'bg-success',
                icon: 'bi bi-emoji-sunglasses-fill',
                text: 'text-success',
                message: mensaje
            })
        }
    }

}
