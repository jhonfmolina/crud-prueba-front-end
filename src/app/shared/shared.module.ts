import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeaderComponent } from './header/header.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { LoaderBootstrapComponent } from './loader-bootstrap/loader-bootstrap.component';
import { ValidFormComponent } from './valid-form/valid-form.component';
import { ToastBootstrapComponent } from './toast-bootstrap/toast-bootstrap.component';


@NgModule({
  declarations: [
    HeaderComponent,
    PageNotFoundComponent,
    SidebarComponent,
    LoaderBootstrapComponent,
    ValidFormComponent,
    ToastBootstrapComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,

  ],
  exports: [
    HeaderComponent,
    RouterModule,
    ReactiveFormsModule,
    SidebarComponent,
    LoaderBootstrapComponent,
    ValidFormComponent,
    ToastBootstrapComponent
  ]
})
export class SharedModule { }
