import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  menuPrincipal= [
    {
      title: 'Gestión',
      marker: 'gestion',
      menuItems : [
        { path: '/dashboard/vistas/productos', title: 'Productos',  icon:'box' },
        { path: '/dashboard/vistas/categorias', title: 'Categorias',  icon:'tag' },
      ]  
    }
  ]


  constructor() { }

  ngOnInit(): void {
    
  }

  

}
