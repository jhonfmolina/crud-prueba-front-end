import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  public estado = new EventEmitter<boolean>();

  constructor() {
  }

  openLoader = ()=> this.estado.emit(true);

  closeLoader = ()=> this.estado.emit(false);

}
