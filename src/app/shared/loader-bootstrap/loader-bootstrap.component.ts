import { AfterViewInit, Component } from '@angular/core';
import { LoaderService } from './loader.service';

@Component({
  selector: 'app-loader-bootstrap',
  templateUrl: './loader-bootstrap.component.html',
  styleUrls: ['./loader-bootstrap.component.scss']
})
export class LoaderBootstrapComponent {

  public estado : boolean = false;

  constructor(private _loaderService : LoaderService){
    this._loaderService.estado.subscribe(
      (data:boolean) => {
        setTimeout(() => {
          this.estado = data;
        }, 0);
      }
    );
  }

  

}
