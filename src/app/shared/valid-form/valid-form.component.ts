import { Component, Input, OnInit } from '@angular/core';
import { Form, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-valid-form',
  templateUrl: './valid-form.component.html'
})
export class ValidFormComponent implements OnInit {

  @Input() validacion: validacion | any;
 
  constructor() { 
   
  }

  ngOnInit(): void {
  }

}

interface validacion {
  formulario: any,
  input: string
}
