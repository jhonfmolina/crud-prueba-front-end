import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../client/servicios/auth.service';
import { ToastService } from '../shared/toast-bootstrap/toast-bootstrap.service';



@Injectable({
  providedIn: 'root'
})
export class TokenizationGuard implements CanActivate {
  constructor(
    private _authService : AuthService,
    private _router: Router,
    private _mensaje: ToastService) {

  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      console.log(this._authService.getAuthorizationToken());
      
      if (!this._authService.getAuthorizationToken()) {
        this._router.navigate(['/']);
        this._mensaje.typeToast('error','No estas autorizado para realizar esta acción.');
        return false;
      }
    return true;
  }
  
}
