import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { SharedModule } from '../shared/shared.module';
import { ClientComponent } from './client.component';


@NgModule({
  declarations: [
    LoginComponent,
    HomeComponent,
    ClientComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class ClientModule { }
