import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public url: string = environment.URL_API;
  constructor(private _http: HttpClient) { }

  postLogin(dataLogin : Login){
    const URL = this.url + `auth/login`;
    return this._http.post<any>(URL, dataLogin)
  }

  setAuthorizationToken(token: TokenModel) {
    localStorage.setItem('access_token', token.access_token)
    localStorage.setItem('refresh_token', token.refresh_token)
  }

  getAuthorizationToken(){
    return localStorage.getItem('access_token');
  }

  deleteAuthorizationToken() {
    localStorage.removeItem('access_token');
    localStorage.removeItem('refresh_token');
  }
  
}

interface Login {
    email:      string,
    password:   string
}

export interface TokenModel {
    access_token: string;
    refresh_token: string;
  }
