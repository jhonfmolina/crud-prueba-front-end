import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastService } from 'src/app/shared/toast-bootstrap/toast-bootstrap.service';
import { AuthService } from '../servicios/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  public formulario: FormGroup;

  constructor(
    private _formBuilder: FormBuilder,
    private _mensaje: ToastService,
    private _router: Router,
    private _authService : AuthService
    ) {
      this.formulario = this._formBuilder.group({
        email:["", [Validators.required, Validators.email]],
        password:["", Validators.required],
      })
     }

  formControl = ()=> this.formulario.controls;

  iniciarSesion() {
    this.formulario.markAllAsTouched();
    this.formulario.updateValueAndValidity();
    if (this.formulario.valid) {  
      this._authService.postLogin(this.formulario.value).subscribe(resp => {
        this._router.navigate(['/dashboard/']);
        this._authService.setAuthorizationToken(resp);
      })
    } else {
      this._mensaje.typeToast("info", this._mensaje.customMsj.verificarForm);
    }
  }
}
