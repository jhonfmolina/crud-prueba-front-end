export class Producto {
    constructor(
        public title:              string,
        public price:              number,
        public description:        string,
        public categoryId:         any,
        public images:             Array<string>,
        public id?:                number
    ){}
}