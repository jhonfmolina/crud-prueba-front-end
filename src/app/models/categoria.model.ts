export class Categoria {
    constructor(
        public name:               string,
        public image:              string,
        public id?:                number
    ){}
}