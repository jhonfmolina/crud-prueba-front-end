import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { TableroComponent } from './vistas/tablero/tablero.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,  
    children: [
      {
        path: '',
        component: TableroComponent
      },
      {
        path: 'vistas',
        loadChildren: () => import('./vistas/vistas.module').then(m => m.VistasModule)
      }
    ]
  },
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
