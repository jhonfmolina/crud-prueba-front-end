import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../client/servicios/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {

  constructor(public _router: Router, private _authService : AuthService){}

  cerrarSesion = ()=> {
    this._authService.deleteAuthorizationToken();
    this._router.navigate(['/']);
  }
}
