import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'src/app/shared/shared.module';
import { VistasRoutingModule } from './vistas-routing.module';
import { ProductosComponent } from './productos/productos.component';
import { CategoriasComponent } from './categorias/categorias.component';
import { TableroComponent } from './tablero/tablero.component';


@NgModule({
  declarations: [
    ProductosComponent,
    CategoriasComponent,
    TableroComponent,
   
  ],
  imports: [
    CommonModule,
    SharedModule,
    VistasRoutingModule
  ]
})
export class VistasModule { }
