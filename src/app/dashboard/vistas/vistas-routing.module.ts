import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TokenizationGuard } from 'src/app/guard/guard-tokenization.guard';
import { CategoriasComponent } from './categorias/categorias.component';
import { ProductosComponent } from './productos/productos.component';
import { TableroComponent } from './tablero/tablero.component';

const routes: Routes = [
  { path:'tablero', component: TableroComponent, title: 'tablero', canActivate: [TokenizationGuard]},
  { path:'productos', component: ProductosComponent, title: 'Productos' , canActivate: [TokenizationGuard]},
  { path:'categorias', component: CategoriasComponent, title: 'categorias', canActivate: [TokenizationGuard]  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VistasRoutingModule { }
