import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Botones } from 'src/app/helpers/btn-accion';
import { Categoria } from 'src/app/models/categoria.model';
import { Producto } from 'src/app/models/producto.model';
import { ToastService } from 'src/app/shared/toast-bootstrap/toast-bootstrap.service';
import { CategoriasService } from '../servicios/categoria.service';
import { ProductosService } from '../servicios/productos.service';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss']
})
export class ProductosComponent implements OnInit, OnDestroy {

  public formulario:                FormGroup;
  public cancelarSubcripcion:       Subscription[] = [];
  public _botones:                  Botones = new Botones();
  public listadoProductos:          Producto[] = []
  public listadoImagenes:           Array<string> = []
  public listadoCategoria:          Categoria[] = []

  constructor(
    private _formBuilder: FormBuilder,
    private _mensaje: ToastService,
    private _productosService: ProductosService,
    private _categoriaService: CategoriasService
  ) {
    this.formulario = this._formBuilder.group({
      _id:          [""],
      title:        ["", Validators.required],
      price:        ["", Validators.required],
      description:  ["", Validators.required],
      categoryId:   ["", Validators.required],
      images:       [""],
    });
    this._botones.ctaInicial();
  }

  formControl = () => this.formulario.controls;

  ngOnDestroy(): void {
    this.cancelarSubcripcion.forEach(subcripcion => subcripcion.unsubscribe());
  }

  ngOnInit(): void {
    this.getListadoCategoria();
    this.cancelarSubcripcion.push(
      this._productosService.refresh.subscribe(() => this.getListadoProductos())
    );

  }

  agregarImagenesProd() {
    if (this.formControl()['images'].value) {
      this.listadoImagenes.push(this.formControl()['images'].value);
      this.formControl()['images'].reset();
    }
    else {
      this._mensaje.typeToast("info", "Debe agregar una imagen.");
    }
  }

  EliminarImagenesProd(posicion: number) {
    this.listadoImagenes.splice(posicion, 1);
  }

  limpiarVista() {
    this.listadoImagenes = [];
    this.formulario.reset();
    this._botones.ctaInicial();
  }

  setEditarProducto(elemento: Producto) {
    let categoryId;
    this._productosService.getProducto(Number(elemento.id)).subscribe(resp => {
      categoryId = resp.category.id
      this.formulario.patchValue({
        _id:          resp.id,
        title:        resp.title,
        price:        resp.price,
        description:  resp.description,
        categoryId:   categoryId,
      });
    })

    this.listadoImagenes = elemento.images;
    this._botones.ctaActualizar();
  }

  dataProducto = () => {
    return new Producto(
      this.formControl()['title'].value,
      this.formControl()['price'].value,
      this.formControl()['description'].value,
      this.formControl()['categoryId'].value,
      this.listadoImagenes,
      this.formControl()['_id'].value
    )
  }

  getListadoCategoria() {
    this.cancelarSubcripcion.push(this._categoriaService.getListadoCategoria().subscribe(resp => {
      this.listadoCategoria = resp;
    }))
  }

  postProducto() {
    this.formulario.markAllAsTouched();
    this.formulario.updateValueAndValidity();
    if (this.formulario.invalid) return this._mensaje.typeToast("info", this._mensaje.customMsj.verificarForm);
    if (this.listadoImagenes.length == 0) {
      this._mensaje.typeToast('info', 'Debe agregar una imagen al producto.');
    } else {
      this.cancelarSubcripcion.push(this._productosService.postProducto(this.dataProducto()).subscribe(resp => {
        this.limpiarVista()
        this._mensaje.typeToast('success', 'Producto creado exitosamente.');
      }))
    }

  }

  putProducto() {
    this.formulario.markAllAsTouched();
    this.formulario.updateValueAndValidity();
    if (this.formulario.invalid) return this._mensaje.typeToast("info", this._mensaje.customMsj.verificarForm);
    if (this.listadoImagenes.length == 0) {
      this._mensaje.typeToast('info', 'Debe agregar una imagen al producto.');
    } else {
      this.cancelarSubcripcion.push(this._productosService.putProducto(this.dataProducto(), this.formControl()['_id'].value).subscribe(resp => {
        this.limpiarVista()
        this._mensaje.typeToast('success', 'Producto actualizado exitosamente.');
      }))
    }

  }

  getListadoProductos() {
    this.cancelarSubcripcion.push(this._productosService.getListadoProductos().subscribe(resp => {
      this.listadoProductos = resp;
    }))
  }

  deleteProducto(elemento? : Producto) {
    let ident = elemento?.id;
    let confirmar = confirm('Esta seguro de eliminar el registro.')
    if (confirmar) {
      this.cancelarSubcripcion.push(this._productosService.deleteProducto(ident ? ident : this.formControl()['_id'].value).subscribe(resp => {
        this.limpiarVista();
        this._mensaje.typeToast('success', 'Producto eliminado exitosamente.');
      }))
    }
  }

}
