import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Categoria } from 'src/app/models/categoria.model';
import { Producto } from 'src/app/models/producto.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoriasService {

  private _refresh = new Subject<void>();
  public url: string = environment.URL_API;
  constructor(private _http: HttpClient) { }

  get refresh() {
    return this._refresh;
  }

  postCategoria(dataCategoria:Categoria){
    const URL = this.url + `categories`;
    return this._http.post<any>(URL, dataCategoria).pipe(
      tap(() => this._refresh.next())
    )
  }

  putCategoria(dataCategoria:Categoria, identificador: string){
    const URL = this.url + `categories/${identificador}`;
    return this._http.put<any>(URL, dataCategoria).pipe(
      tap(() => this._refresh.next())
    )
  }

  getListadoCategoria() {
    const URL = this.url + `categories`;
    return this._http.get<Categoria[]>(URL)
  }

  getCategoria(identificador: number) {
    const URL = this.url + `categories/${identificador}`;
    return this._http.get<Categoria>(URL)
  }

  deleteCategoria( identificador: string){
    const URL = this.url + `categories/${identificador}`;
    return this._http.delete<any>(URL).pipe(
      tap(() => this._refresh.next())
    )
  }

  
}
