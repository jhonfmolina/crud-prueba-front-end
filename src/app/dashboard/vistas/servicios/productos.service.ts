import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Producto } from 'src/app/models/producto.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  private _refresh = new Subject<void>();
  public url: string = environment.URL_API;
  constructor(private _http: HttpClient) { }

  get refresh() {
    return this._refresh;
  }

  postProducto(dataProducto:Producto){
    const URL = this.url + `products`;
    return this._http.post<any>(URL, dataProducto).pipe(
      tap(() => this._refresh.next())
    )
  }

  putProducto(dataProducto:Producto, identificador: string){
    const URL = this.url + `products/${identificador}`;
    return this._http.put<any>(URL, dataProducto).pipe(
      tap(() => this._refresh.next())
    )
  }

  getListadoProductos() {
    const URL = this.url + `products`;
    return this._http.get<Producto[]>(URL)
  }

  getProducto(identificador: number) {
    const URL = this.url + `products/${identificador}`;
    return this._http.get<any>(URL)
  }

  deleteProducto( identificador: string){
    const URL = this.url + `products/${identificador}`;
    return this._http.delete<any>(URL).pipe(
      tap(() => this._refresh.next())
    )
  }

  
}
