import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Botones } from 'src/app/helpers/btn-accion';
import { Categoria } from 'src/app/models/categoria.model';
import { ToastService } from 'src/app/shared/toast-bootstrap/toast-bootstrap.service';
import { CategoriasService } from '../servicios/categoria.service';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.scss']
})
export class CategoriasComponent implements OnInit, OnDestroy {

  
  public formulario :             FormGroup;
  public cancelarSubcripcion :    Subscription[] = [];
  public _botones :               Botones = new Botones();
  public listadoCategorias:        Categoria[] = []

  constructor(
    private _formBuilder: FormBuilder,
    private _mensaje: ToastService,
    private _categoriaService: CategoriasService
  ) {
    this.formulario = this._formBuilder.group({
      _id:                  [""],
      name:                 ["", Validators.required],
      image:                ["", Validators.required],
    });
    this._botones.ctaInicial();
  }

  formControl = () => this.formulario.controls;

  ngOnDestroy(): void {
    this.cancelarSubcripcion.forEach(subcripcion => subcripcion.unsubscribe());
  }

  ngOnInit(): void {
    this.cancelarSubcripcion.push(
      this._categoriaService.refresh.subscribe(() => this.getListadoCategoria())
    );
  }

  dataCategoria = ()=> {
    return new Categoria(
      this.formControl()['name'].value,
      this.formControl()['image'].value,
      this.formControl()['_id'].value
    )
  }

  limpiarVista() {
    this.formulario.reset();
    this._botones.ctaInicial();
  }

  setEditarCategoria(elemento: Categoria) {
    if (!elemento.id) return;
    this._categoriaService.getCategoria(elemento.id).subscribe(resp => {
      this.formulario.patchValue({
        _id: resp.id,
        name: resp.name,
        image: resp.image
      });
    })
    this._botones.ctaActualizar();
  }

  postCategoria() {
    this.formulario.markAllAsTouched();
    this.formulario.updateValueAndValidity();
    if (this.formulario.invalid) return this._mensaje.typeToast("info", this._mensaje.customMsj.verificarForm);
    this.cancelarSubcripcion.push(this._categoriaService.postCategoria(this.dataCategoria()).subscribe(resp => {
      this.limpiarVista();
      this._mensaje.typeToast('success', 'Categoria creada exitosamente.');
    }))
  }

  putCategoria() {
    this.formulario.markAllAsTouched();
    this.formulario.updateValueAndValidity();
    if (this.formulario.invalid) return this._mensaje.typeToast("info", this._mensaje.customMsj.verificarForm);
    this.cancelarSubcripcion.push(this._categoriaService.putCategoria(this.dataCategoria(), this.formControl()['_id'].value).subscribe(resp => {
      this.limpiarVista();
      this._mensaje.typeToast('success', 'Categoria actualizada exitosamente.');
    }))
    
  }

  getListadoCategoria() {
    this.cancelarSubcripcion.push(this._categoriaService.getListadoCategoria().subscribe(resp => {
      this.listadoCategorias = resp;
    }))
  }

  deleteCategoria(elemento? : Categoria) {
    let ident = elemento?.id;
    let confirmar = confirm('Esta seguro de eliminar el registro.')
    if (confirmar) {
      this.cancelarSubcripcion.push(this._categoriaService.deleteCategoria(ident ? ident : this.formControl()['_id'].value).subscribe(resp => {
        this.limpiarVista();
        this._mensaje.typeToast('success', 'Categoria eliminada exitosamente.');
      }))
    }
  }

}
