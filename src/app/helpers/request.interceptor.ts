import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpResponse,
} from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { LoaderService } from '../shared/loader-bootstrap/loader.service';
import { ToastService } from '../shared/toast-bootstrap/toast-bootstrap.service';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {
  errorMessage: string = '';
  constructor(
    public _router: Router, private _loaderService : LoaderService,
    private _mensaje : ToastService
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    this._loaderService.openLoader();

    return next.handle(req).pipe(
      map((respuesta: HttpEvent<any>) => {
        if (respuesta instanceof HttpResponse) {
          if (respuesta.status == 200 && respuesta.body) {
            !environment.production ? console.log(respuesta.body) : false;
            if (respuesta.body.data instanceof Array) {
              if (respuesta.body.data.length == 0) {
                this._mensaje.typeToast('info', `No se encontraron registros, --${respuesta.body.message}--`);
              }
            }
          }
          this._loaderService.closeLoader();
        }
        return respuesta;
      }),
      catchError((error: HttpErrorResponse) => {
               
        switch (error.status) {
          case 0:
            this.errorMessage = 'Problemas con la conexión al servidor, debe comunicarse con soporte tecnico.';
            break;
          case 400:
            this.errorMessage = error.error.message;          
            break;
          case 401:
            this.errorMessage = 'No estas autorizado para realizar esta acción.';
            this._router.navigate(['/login']);
            break;
          case 404:
            this.errorMessage = 'El contenido que solicitas no se encuentra disponible.';
            break;
          case 422:
            this.errorMessage = 'El contenido o registro ya existe.';
            break;
          case 500:
            this.errorMessage = 'La solicitud al servidor, no pudo ser completada.';
            break;
          default:
            break;
        }
        console.log(error);
        this._mensaje.typeToast('error', this.errorMessage);
        this._loaderService.closeLoader();
        return throwError(`Estado: ${error.status}, Mensaje: ${this.errorMessage}, url: ${error.url}`);
        // return throwError(error);
      })
    );
  }
}
