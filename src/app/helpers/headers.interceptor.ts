
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpInterceptor,
} from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Router } from '@angular/router';
import { AuthService } from '../client/servicios/auth.service';

@Injectable()
export class HeadersInterceptor implements HttpInterceptor {
  
  constructor( private _router: Router, private _authService : AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {

    const authToken = this._authService.getAuthorizationToken();

    let request = req;

    if (
      this._router.url.includes('login') ||
      this._router.url.includes('register')
    ) {
      return next.handle(req).pipe(catchError((error) =>  throwError(error)));
    }

    if (authToken) {
      request = req.clone({
        headers: req.headers
          .set('Authorization', `Bearer ${authToken}`)
          .set('Content-Type', 'application/json')
          .set('Accept', 'application/json'),
      });
    }
    return next.handle(request);
  }
}
