import { FormGroup } from "@angular/forms";

export class Botones {

    botones: btn = {
        btnGuardar: false,
        btnActualizar: false,
        btnAnular: false,
        btnListado: false
    }

    constructor() {}

    ctaInicial = () => (
        this.botones = {
            btnGuardar: true,
            btnActualizar: false,
            btnAnular: false,
            btnListado: true
        }
    )

    ctaActualizar= () => (
        this.botones = {
            btnGuardar: false,
            btnActualizar: true,
            btnAnular: false,
            btnListado: true
        }
    )

    ctaAnular= () => (
        this.botones = {
            btnGuardar: false,
            btnActualizar: false,
            btnAnular: true,
            btnListado: false
        }
    )

    limpiarVista= (list:Array<any>,  form:FormGroup, dataSource:any = [])=> {
        list = []; 
        dataSource.data = []; 
        this.ctaInicial();
        form.reset();
    }
    
}

interface btn {
    btnGuardar: boolean,
    btnActualizar: boolean,
    btnAnular: boolean,
    btnListado: boolean
}
