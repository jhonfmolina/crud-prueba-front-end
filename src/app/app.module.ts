import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DEFAULT_CURRENCY_CODE, LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClientModule } from './client/client.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { HeadersInterceptor } from './helpers/headers.interceptor';
import { RequestInterceptor } from './helpers/request.interceptor';
import { SharedModule } from './shared/shared.module';

// Fechas en idioma español
import localeEs from "@angular/common/locales/es";
import { registerLocaleData } from "@angular/common";
registerLocaleData(localeEs, "es")

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    SharedModule,
    ClientModule,
    DashboardModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: "es" },
    { provide: DEFAULT_CURRENCY_CODE, useValue: 'COP' },
    { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true},
    { provide: HTTP_INTERCEPTORS, useClass: HeadersInterceptor, multi: true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
